num1 = int(input("Dame un número entero no negativo: "))
num2 = int(input("Dame otro: "))


if num1 <= 0:
    print("Tiene que ser un número entero positivo")
elif num2 <= 0:
    print("Tiene que ser un número entero positivo")


else:
    sum_imp = 0
    for num in range(num1, num2 + 1):
        if num % 2 != 0:
            sum_imp += num
        print("La suma de los números impares entre", num1, "y", num2, "es", sum_imp)
